"# PyBites Challenges README" 
This repository is for completing the challenges provided by PyBites (https://pybit.es/pages/challenges.html).

Challenges will be kept in separate branches off develop so that the Git Branching Model is practiced and individual challenges can have pull requests submitted.